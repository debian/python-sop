Source: python-sop
Section: python
Priority: optional
Maintainer: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 mypy <!nocheck>,
 python3-all (>= 3.7),
 python3-setuptools,
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/debian/python-sop.git
Vcs-Browser: https://salsa.debian.org/debian/python-sop
Homepage: https://gitlab.com/dkg/python-sop
Rules-Requires-Root: no

Package: python3-sop
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: Framework for implementing the Stateless OpenPGP CLI in Python
 The Stateless OpenPGP Command-Line Interface (or `sop`) is a
 specification that encourages OpenPGP implementors to provide a
 common, relatively simple command-line API for purposes of object
 security.
 .
 This Python module helps implementers build such a CLI from any
 implementation accessible to the Python interpreter.
 .
 This package does *not* provide such an implementation itself -- this
 is just the scaffolding for the command line, which should make it
 relatively easy for an implementer to supply a handful of Python
 functions as methods of a class and get a full-fledged CLI as the
 result.
